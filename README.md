# Removable platforms

Removable platforms act as a more versatile form of landfill since they can be removed after use and be put into blueprints.

Xterminator made a spotlight on this mod, http://www.youtube.com/watch?v=PQKvzBvEm4A

Notable Quirks:
1. Platforms are immortal while a building is on top, but the player dies if a platform is removed from underneath them.
2. Platforms can be overwritten by other tile types if you build them on top, so you can texture the water how you like.
3. Platforms also have the advantage that they can be blueprinted or deconstructed with bots.
4. There is a mod setting that lets you change the texture of the platform. There are currently two themes.
5. Platforms provide a 30% speed boost
6. You can use NUM+ and NUM- to control the placement size. (This is a vanilla feature of all tiles.)
